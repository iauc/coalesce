# vim: set noexpandtab fo-=t:
# https://www.gnu.org/software/make/manual/make.html
.PHONY: default
default:

########################################################################
# boiler plate
########################################################################
SHELL=bash
current_makefile:=$(lastword $(MAKEFILE_LIST))
current_makefile_dirname:=$(dir $(current_makefile))
current_makefile_dirname_abspath:=$(dir $(abspath $(current_makefile)))
current_makefile_dirname_realpath:=$(dir $(realpath $(current_makefile)))

ifneq ($(filter all vars,$(VERBOSE)),)
dump_var=$(info var $(1)=$($(1)))
dump_vars=$(foreach var,$(1),$(call dump_var,$(var)))
else
dump_var=
dump_vars=
endif

ifneq ($(filter all targets,$(VERBOSE)),)
__ORIGINAL_SHELL:=$(SHELL)
SHELL=$(warning Building $@$(if $<, (from $<))$(if $?, ($? newer)))$(TIME) $(__ORIGINAL_SHELL)
endif


########################################################################
# variables ...
########################################################################

GOPATH:=$(shell type go >/dev/null 2>&1 && go env GOPATH)
export PATH:=$(if $(GOPATH),$(GOPATH)/bin:,)$(PATH)

########################################################################
# targets ...
########################################################################

.PHONY: all
default: all
all:

.PHONY: nothing
nothing:
	@echo doing $(@) ...


tools_dir=tools
cache_dir=$(HOME)/.cache
modd_bin=$(tools_dir)/modd

$(modd_bin): | $(tools_dir)/ $(cache_dir)/
	cd $(cache_dir) && wget -c https://github.com/cortesi/modd/releases/download/v0.8/modd-0.8-linux64.tgz
	tar -zxvf $(cache_dir)/modd-0.8-linux64.tgz --strip-components=1 -C $(tools_dir)

.PHONY: distclean
distclean: clean-$(tools_dir)/
	rm -vf coverage.out

.PHONY: toolchain
toolchain: $(modd_bin)
	cd ~/ && go get $(if $(filter all commands,$(VERBOSE)),-v) $(go_get_flags) \
		github.com/golangci/golangci-lint/cmd/golangci-lint@v1.39.0 \
		&& true

.PHONY: toolchain-update
toolchain-update: go_get_flags+=-u
toolchain-update: toolchain

.PHONY: validate-static
validate-static:
	golangci-lint run $(if $(filter all commands,$(VERBOSE)),-v) ./...

.PHONY: validate-fix
validate-fix:
	golangci-lint run $(if $(filter all commands,$(VERBOSE)),-v) --fix ./...

.PHONY: test
test:
	go test $(if $(filter all commands,$(VERBOSE)),-v) -cover -race -coverprofile=coverage.out -covermode=atomic ./...

.PHONY: validate-dynamic
validate-dynamic: test

.PHONY: validate
validate: validate-static validate-dynamic

do_validate=$(if $(skip_validate),,validate)

.PHONY: watch
watch: $(modd_bin)
	./$(modd_bin) --debug --notify --file modd.conf

.PHONY: build
all: build
build: $(do_validate)
	go build ./...

.PHONY: install
install: $(do_validate)
	go install ./...


$(eval $(shell cat .env default.env 2>/dev/null \
    | sed -E '/^\s*#.*/d' \
    | tr '\n' '\000' \
    | sed -z -E 's/^([^=]+)=(.*)/\1\x0\2/g' \
    | xargs -0 -n2 bash -c 'printf "export %s=%s\n" "$${@}"' /dev/null | tee /dev/stderr))

$(call dump_vars,CODECOV_TOKEN)

.PHONY: publish-coverage
publish-coverage: test
	bash <(curl -s https://codecov.io/bash)

help:
	@printf "########################################################################\n"
	@printf "TARGETS:\n"
	@printf "########################################################################\n"
	@printf "%-32s%s\n" "help" "Show this output ..."
	@printf "%-32s%s\n" "all" "Build all outputs (default)"
	@printf "%-32s%s\n" "toolchain" "Install toolchain"
	@printf "%-32s%s\n" "validate" "Validate everything"
	@printf "%-32s%s\n" "validate-fix" "Fix auto-fixable validation errors"
	@printf "%-32s%s\n" "build" "Build everything"
	@printf "%-32s%s\n" "install" "Install everything"
	@printf "%-32s%s\n" "distclean" "Cleans all things that should not be checked in"
	@printf "########################################################################\n"
	@printf "VARIABLES:\n"
	@printf "########################################################################\n"
	@printf "%-32s%s\n" "VERBOSE" "Sets verbosity for specific aspects." \
						"" "Space seperated." \
						"" "Valid values: all, vars, commands, targets"

########################################################################
# useful ...
########################################################################
## force ...
.PHONY: .FORCE
.FORCE:
$(force_targets): .FORCE

## dirs ...
.PRECIOUS: %/
%/:
	mkdir -vp $(@)

.PHONY: clean-%/
clean-%/:
	@{ test -d $(*) && { set -x; rm -vr $(*); set +x; } } || echo "directory $(*) does not exist ... nothing to clean"
